# Debian stable based golang image
FROM golang:latest

# clone and compile hugo
RUN go get github.com/gohugoio/hugo

# clone hugo example site
RUN git clone https://github.com/gohugoio/hugoBasicExample.git

# source
RUN git clone https://gitlab.com/redster/mathys.io.git
# builds
RUN git clone https://gitlab.com/redster/blog.mathys.io.git
RUN git clone https://gitlab.com/redster/photography.mathys.io.git
RUN git clone https://gitlab.com/redster/sandro.mathys.io.git
